const express = require('express');
const https = require('https');
const fs = require('fs');

const app = express();

const PORT = 443;

const options = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
};

https.createServer(options, app).listen(PORT, ()=>{
    console.log('Se ha conectado https PORT ' + PORT)
});


app.get('/', function(req, res){
    res.send('Hola estas conectado con esta pagina');
    console.log('Se ha recibido dicha petición ' + PORT);
})



